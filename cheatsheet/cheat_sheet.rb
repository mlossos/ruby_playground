# Ruby Cheat Sheet
# @author Michael Lossos
# -- I'm a Ruby n00b.
#
# References:
# Old Ruby 1.4 syntax: http://www.ruby-doc.org/docs/ruby-doc-bundle/Manual/man-1.4/syntax.html
# Hunt Thomas book: http://www.ruby-doc.org/docs/ProgrammingRuby/html/index.html
# Blocks, Procs, Lambdas: http://www.robertsosinski.com/2008/12/21/understanding-ruby-blocks-procs-and-lambdas/
# http://blog.peepcode.com/tutorials/2010/what-pythonistas-think-of-ruby
# Ruby best practices: http://rubybestpractices.com/    http://sandal.github.com/rbp-book/pdfs/rbp_1-0.pdf

# TODO Examples of Hashes
# TODO Examples of Regexp groups, replace all in str, iterate to next match
# TODO Examples of: map, filter, each, each_with_index, and itertools "equivalents" (Array#combination, Enumerable#group_by)
# TODO Examples of Equals eql? ==, case when ===, and operator precedence  # http://www.ruby-doc.org/docs/ProgrammingRuby/html/tut_expressions.html
# TODO Examples of Implicit/special variables in while file.gets and the line num special var $
# TODO Examples of Arrays, iterators, array slicing / filtering

# For another guide:
# TODO File examples, load csv/tab delim files
# TODO json parsing examples

require 'pathname'  # imports the ruby script pathname.rb (from ruby standard lib) only once. See also load().

$my_global_var = true # global (ewww)

=begin
  This is a multiline comment.
=end
module Stem # Modules are like namespaces, not classes
  STEM_COLOR = "black" # A constant by convention (const-ness not enforced)

  # Ruby indent convention: 2 spaces (unlike Python's 4 spaces)
  def pluck_stem()
    puts("Plucked out the stem!")
  end
end

module Skin
    def peel_skin()
      puts("Peeled off the skin!")
    end
end

module FruitInheritanceExamples
  class Fruit
    # Default accessibility level is public

    def initialize(name) # Constructor with param
      @name = name # private var name
      # self is this object
      # "some str with #{variable} uses var subst in the str"
      # @var refs a private var in the object
      puts("Created a #{self.class} named #{@name}!")
    end

    # When documenting methods, use hash for instance methods like Fruit#print_fruit_info and dot for class methods like Object.eql?
    def print_fruit_info()
      puts("Fruit info: #{@name}")
    end

    def to_s() # Override Object.to_s aka to string. See also inspect().
      return @name
    end
  end

  class Apple < Fruit   # Apple subclass of Fruit. Multiple inheritance NOT supported (use mixins instead).
    attr_accessor :name # Expose private @name as an accessor (property) called name. The colon : is used for names/labels.
    attr_accessor :num_seeds

    # Mix-ins (mixins, mix ins)
    include Stem # Stem module methods/variables become object instance methods for Apples, e.g. a = Apple.new; a.pluck_stem()
    extend Skin # Skin module becomes CLASS methods for Apples, e.g. Apple.peel_skin()

    def initialize(name, num_seeds=0, more_info=nil) # Constructor, with default param more_info (nil is Python None, Java null)
      super(name) # Call base constructor
      @num_seeds = num_seeds
      @more_info = more_info
    end

    def print_fruit_info() # Override method
      # Do not call self.get_apple_info or you'll get: private method `get_apple_info' called for red fuji apple:FruitInheritanceExamples::Apple (NoMethodError)
      apple_info = get_apple_info # Method invocation parens () optional
      print(super) # Call superclass method via super
      puts(" " + apple_info) # puts has a newline, print doesn't.
    end

    protected # Below this is protected

    def get_apple_info()
      return get_apple_info_impl
    end

    private # Below this is private

    def get_apple_info_impl()
      return "(It's an Apple!)"
    end
  end

  class FujiApple < Apple
    def is_super_sweet?() # The question mark ? is a convention to indicate the method returns true/false
      return true
    end
  end
end


module ExceptionExamples
  extend self # Trick that makes all public module methods accessible via ModuleExamples.method_name

  def all()
    catch_throw_example
    raise_rescue_example
  end

  private # private module methods only accessible within the module (not exposed by extend self).

  def catch_throw_example()
    song_list = ['song1', 'song2', 'bad song3', 'song4']
    songs_to_play = []
    catch (:done) do # :done is a label that throw will unwind the stack to.
      for song in song_list
        throw :done if song == 'bad song3' # exit the loop (unwind) if ...
        songs_to_play << song   # Append song to array, same as push() ?
      end
    end
    for song in songs_to_play
      puts "#{song}, "
    end
  end

  def raise_rescue_example()
    begin
      # do stuff
      raise ArgumentError, 'Raising an exception', self
    rescue ArgumentError => err # handle a specific exception
      puts 'Caught ' + err
    rescue Exception # catching the base of all exceptions is possibly dangerous
      puts 'Caught anything else.'
      # See also: retry
    ensure
      puts 'Always executed.'
    end
  end
end

module ModuleExamples
  extend self

  def f()
    puts 'f() called.'
  end

  def g()
    puts 'g() called'
  end

  def h(param1)
    puts "h(#{param1}) called"
    puts 'This is not interpolating param1: h(#{param1})' # Using ' for string does not substitute variables (called interpolation).
  end
end

module ModuleExamples2
  def f()
    puts 'f()'
  end
  module_function :f    # Expose only f as a module function, invoked as:   ModuleExamples2.f
end

module RegexpExamples
  extend self

  def all()
    if /he.*o/ =~ 'hello'
      puts 'Matched hello.'
    end
    matcher = Regexp.new('he+llo')
    to_match = ['heeeeeeeeeello', 'hllo']
    for str in to_match
      puts "Match #{str}? #{matcher.match(str)}"
    end
  end
end

module MethodExamples
  extend self

  def all()
    foo = Foo.new
    foo.f
    puts "foo.is_foo?: #{foo.is_foo?}"

    Foo.class_method

    begin
      foo.class_method # Cannot call class methods via object instance (unlike Java)
    rescue
      # swallow
    end
  end


  private
  class Foo
    def initialize()
      @special_data = "My special data"
    end

    def f() # instance method
      print 'f() called'
    end

    def is_foo?()
      return true
    end

    def modify_me!() # The ! is a convention to indicate a dangerous method (modifies an object in place)
      return @special_data.capitalize!
    end

    def self.class_method()  # Class methods (static in Java)
      puts 'class_method()'
    end
  end
end

class Array # Adds to standard library Array (Ruby's classes are "open classes" that can be modified)
  def my_iterate!(code)
    self.each_with_index do |n, i|
      self[i] = code.call(n)
    end
  end
end

module BlockProcLambdaExamples
  extend self

  def all()
    modify_array_lambda
    modify_array_block
    modify_array_proc
  end

  private

  # Borrowed from Robert Sosinski: http://www.robertsosinski.com/2008/12/21/understanding-ruby-blocks-procs-and-lambdas/
  def modify_array_lambda()
    data = [1, 2, 3, 4]
    # The |n| pipes delimit the param list to lambdas and blocks
    data.my_iterate!(lambda { |n| n ** 2 }) # lambda is a one time use func, no way to ref lambda outside of declaration (see Proc below).
    puts data.inspect # => [1, 4, 9, 16]
  end

  def modify_array_block()
    array = [1, 2, 3, 4]
    array.collect! do |n|
      n ** 2
    end
    puts array.inspect
  end

  def modify_array_proc()
    array_1 = [1, 2, 3, 4]
    array_2 = [2, 3, 4, 5]

    square = Proc.new do |n|
      n ** 2
    end

    array_1.my_iterate!(square) # Can reuse the method
    array_2.my_iterate!(square)

    puts array_1.inspect # => [1, 4, 9, 16]
    puts array_2.inspect # => [4, 9, 16, 25]
  end
end

module DuckTypingExamples
  extend self

  def all()
    include FruitInheritanceExamples
    apples = [Apple.new("green apple"), FujiApple.new("fuji apple")]
    for apple in apples
      if apple.respond_to? :is_super_sweet? # Capability testing (duck typing) using respond_to?
        puts "apple of type #{apple.class} has is_super_sweet?: #{apple.is_super_sweet?}"
      end
    end
  end
end

module CheatSheetMain
  def run_inheritance_examples()
    puts '____________________________________ FruitInheritanceExamples'
    include FruitInheritanceExamples # All classes declared in module are now in this scope, e.g. Apple
    apple = Apple.new("red fuji apple", 0)
    puts "apple = #{apple}"
    apple.print_fruit_info()
    puts "Num seeds #{apple.num_seeds}"

    begin
      apple.get_apple_info() # invoke a private method
    rescue NoMethodError => no_method_err
      puts "Caught expected exception: #{no_method_err}"
    end

    apple.pluck_stem() # instance mixin
    Apple.peel_skin() # class mixin
  end

  def run_module_examples()
    puts '____________________________________ ModuleExamples'
    # Module method calls:
    ModuleExamples.f # Method invocation does not require ()
          ModuleExamples.g # Indentation is irrelevant, unlike Python
    ModuleExamples::g() # Can also use :: for method invocation.
    ModuleExamples.h 'p1val' # Parens () are usually optional on method invocation.

    ModuleExamples2.f
  end

  def begin_examples()
    puts '____________________________________ Examples'
    puts "#{__FILE__}:#{__LINE__}"
    puts "pwd: #{Pathname.getwd}"
    puts "global: #{$my_global_var}"
  end

  def main()
    begin_examples
    run_inheritance_examples
    run_module_examples

    puts '____________________________________ ExceptionExamples'
    ExceptionExamples.all

    puts '____________________________________ RegexpExamples'
    RegexpExamples.all

    puts '____________________________________ MethodExamples'
    MethodExamples.all

    puts '____________________________________ BlockProcLambdaExamples'
    BlockProcLambdaExamples.all

    puts '____________________________________ DuckTypingExamples'
    DuckTypingExamples.all
  end
end

# If we want to import cheat_sheet from another module (no need to in same file):
#require 'cheat_sheet'
include CheatSheetMain
main



