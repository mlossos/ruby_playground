#################### Begin code snippet

POP_VENDING_MACHINE = {
    'Cool Cola' => 3.59,
    'Mighty Mist' => 4.27,
    'Butter Beer' => 5.99
}

def vend_pop(pop_selection, money_amount_paid)
    """
    Vends pop given a str name of the pop and sufficient money.
    Return the change, if overpaid.
    """
    amount_paid = money_amount_paid.to_f
    if amount_paid <= 0
        raise ArgumentError, 'Please insert coins. No freebies!'
    end
    if not POP_VENDING_MACHINE.has_key?(pop_selection)
        raise ArgumentError, "Invalid selection: #{pop_selection}"
    end

    cost = POP_VENDING_MACHINE[pop_selection]
    puts "Here's some pop: #{pop_selection}"
    if amount_paid < cost
        raise ArgumentError, "Not enough coins. Insert #{cost - amount_paid} more!"
    end
    return amount_paid - cost
end


################### end

puts vend_pop(ARGV[0], ARGV[1])
